# Project Title

"Pokedex App" is a directory application to pokemon. Display pokemon cards, research a pokemon by name or by type are the mainly functions of the application.

This application use an open API that you can access [HERE](https://pokemontcg.io/)

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

1- Install Nodejs 10.15.1

```
https://nodejs.org/en/download/
```

2- Install Ionic 3 with Cordova

```
npm install -g ionic@3 cordova
```

3- Plugins	

In app/src/app.module.ts, HttpClientModule and HttpClient has been imported. 

### Installing

To lauch application on computer thank of Ionic lab, go the directory of the project and run the following command :

```
ionic serve -lc
```

To launch application on your device (for example an Android) :

First, you must add Android platform 

```
ionic cordova platform add android
```

Then, you must build :

```
ionic cordova build android
```

Then, application can be lauched with the following command :

```
ionic cordova run android
```

## Screenshots application

In "screenshots directory", there are four screenshot of the application.

```
"HomePage" --> It's the home page of application, at the top there are the serach bar, that is used to search pokemon by name
			   Under that, there are the "synchonization button" to synchronize pokemon list with the type pokemon chosen in OptionPage
			   And just under that, the pokemon list according to option and bar reasearch.
```
```		   
"AboutPage" --> It's the page of information pokemon. When you click on a pokemon name in list pokemon of HomePage, AboutPage is 
				opened and you can see the card pokemon
```
```
"Research" --> It's an exemple of adaptation of list pokemon when letter are entered in searchBar
```
```
"OptionPage --> It's the option page, where user can choice the pokemon type   
```

## Running the tests

There are no automated tests

## Built With

* [Ionic](https://ionicframework.com/) - The mainly framework used, the version 3 has been used

## Authors

* **Guilloux Grégoire** - *Initial work* - [GitLab link profil](https://gitlab.com/guilloux.gregoire)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Thank to Ilyes Sghir to delivered me an course on ionic framework at Paris Descartes University