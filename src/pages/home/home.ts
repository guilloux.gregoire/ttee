import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import {AboutPage} from '../about/about';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  
  pokemons: Observable<any>;

 
  constructor(public navCtrl: NavController, public navParams: NavParams, private httpClient: HttpClient) { 
    this.pokemons = this.httpClient.get('https://api.pokemontcg.io/v1/cards');  
  }
  openDetails(pokemon) {
    this.navCtrl.push(AboutPage, {pokemon:pokemon});
}
}
