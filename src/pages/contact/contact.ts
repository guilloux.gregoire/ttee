import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import {AboutPage} from '../about/about';

@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})
export class ContactPage {

  pokemons: Observable<any>;
  listPokemonClean:string[]=[];
  listPokemon:string[]=[];
  namePokemon:string;
  ca:[any];
  va:[any];
  
  constructor(public navCtrl: NavController, public navParams: NavParams, private httpClient: HttpClient) {  
    this.httpClient.get('https://api.pokemontcg.io/v1/cards').subscribe(data => {
    this.ca = (data['cards']);
    this.ca.forEach(element => {
      this.va = element;
      this.listPokemon.push(this.va.name);
      this.listPokemonClean.push(this.va.name);
    });
    console.log(this.listPokemon);
});
}

initializeItems(): void {
  this.listPokemon=this.listPokemonClean;
}


getItems(searchbar) {
  // Reset items back to all of the items
  this.initializeItems();

  // set q to the value of the searchbar
  var q = searchbar.srcElement.value;


  // if the value is an empty string don't filter the items
  if (!q) {
    return;
  }

  this.listPokemon = this.listPokemon.filter((v) => {
    if(v && q) {
      if (v.toLowerCase().indexOf(q.toLowerCase()) > -1) {
        return true;
      }
      return false;
    }
  });

  console.log(q, this.listPokemon.length);

}

}
